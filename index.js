

class Cart{

    constructor(){
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product,qty){
        this.contents.push({product: product,quantity: qty});
        this.computeTotal();
        return this;        

    }

    showCartContents(){
        return this.contents
    }

    clearCartContents(){
        this.contents = [];
        return this
    }

    computeTotal(){
        let total = 0;

        this.contents.forEach(content => {

         let result = content.quantity * content.product.price
         total += result
        })
        this.totalAmount = total;
        return this;
    }
}

class Product{

    constructor(name,price){
        this.name = name;
        this.price = price;
        this.isActive = true;
    }

    archive(){
        this.isActive = false;
        return this;
    }

    updatePrice(price){
        this.price = price;
        return this;
    }

}

class Customer{

    constructor(email,cart){
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    checkOut(){
        this.cart.computeTotal();
        if(this.cart !== null){ 
            this.orders.push({products:this.cart.contents, totalAmount:this.cart.totalAmount})
            console.log(this.cart)
        }

        
    }
}


const cart1 = new Cart();
console.log(cart1);

const prodA = new Product("soap", 9.99);
const prodB = new Product("shampoo", 9.99);
console.log(prodA);
console.log(prodB);

const john = new Customer('john@mail.com');
console.log(john);